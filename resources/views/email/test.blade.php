<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <h1>Письмо с сайта - Турниры по футболу</h1>
    @if ($tournament)
        <h2>Поздравляем, Вы приглашены участвовать в Турнире {{$tournament->title}}</h2>

        <h3>Игра состоится {{$tournament->start_game}}</h3><br>

        <h4>Место - {{$tournament->destination}}</h4>

        <h5>Более подробно - переходите на сайт - http://champion/</h5>
    @endif
</head>
<body>

</body>
</html>
