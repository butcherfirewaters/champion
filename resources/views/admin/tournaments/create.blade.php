@extends('admin.layouts.layout')

@section('content')
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Новая игра</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Blank Page</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Новая игра по футболу</h3>
                        </div>
                        <!-- /.card-header -->

                        <form role="form" method="post" action="{{ route('tournaments.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="title">Название</label>
                                    <input type="text" name="title" value="{{old('title')}}"
                                           class="form-control @error('title') is-invalid @enderror" id="title"
                                           placeholder="Например, игра по футболу 5на5 - 3-и команды">
                                </div>

                                <div class="form-group">
                                    <label for="destination">Где будет проходить игра</label>
                                    <textarea name="destination" class="form-control @error('destination') is-invalid @enderror" id="destination" rows="3" placeholder="Место встречи...">{{isset($tournament->destination) ? $tournament->destination  : old('destination')}}</textarea>
                                </div>

                                <div class="form-group row">
                                    <label for="start_game" class="col-4 col-form-label">Начало игры</label>
                                    <div class="col-8">
                                        <input class="form-control" type="datetime-local" value="{{date("Y-m-d").'T'.date("H:i")}}" id="example-datetime-local-input" name="start_game">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="user_id">Организатор турнира</label>
                                    <select class="form-control custom-select @error('user_id') is-invalid @enderror" id="user_id" name="user_id">
                                        <option name="user_id" value="{{ $user->id }}">{{ $user->name }}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="LatLng">Выберите на карте, где будет проходить соревнование</label>
                                    <div id="mapid"></div>
                                    <input type="hidden" id="Lng" name="Lng">
                                    <input type="hidden" id="Lat" name="Lat">
                                </div>

                                <div class="form-group">
                                    <label>Добавьте игроков</label>
                                    <div class="select2-purple">
                                        <select name="players[]" id="players" class="select2" multiple="multiple" data-placeholder="Выберите игроков"
                                                data-dropdown-css-class="select2-purple" style="width: 100%;">
                                            @foreach ($players as $k => $player)
                                                <option value="{{$player->id}}">{{$player->name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                                <!-- /.form-group -->

                                <div class="form-group">
                                    <label for="how_many_peoples">Какое максимальное количество игроков может принимать участие в игре? (Минимум 8 человек - максимум 20</label>
                                    <br>
                                    <input type="number" id="how_many_peoples" name="how_many_peoples" value="8"
                                           min="8" max="20">
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form>

                    </div>
                    <!-- /.card -->

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <script>

        var mymap = L.map('mapid').setView([47.91039145, 33.39174271], 16);

        var tileLayer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 16,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoiZmlyZXdhdGVycyIsImEiOiJja2lrMjN2YnEwNXBoMnNtODhoaTFsZGNlIn0.KHdxZaUaeGjneuxMLMyzxw'
        });
        tileLayer.addTo(mymap);

        mymap.on('click', addMarker); // function по клику

        let newMarker = {};

        // Creating a Marker Options
        let markerOptions = {
            title: "MyLocation",
            clickable: true,
            /*draggable: true*/
        };

        function addMarker(e) {
            //remove previous markers
            console.log(newMarker);
            if (newMarker) {
                mymap.removeLayer(newMarker);
            }
            // Add marker to map at click location; add popup window
            newMarker = new L.marker(e.latlng, markerOptions).bindPopup('Твоя локация').addTo(mymap);
            UpdateLatLng(e);
        }

        function UpdateLatLng(e) {
            let latLngValue;
            latLngValue = e.latlng;
            console.log(latLngValue);
            console.log(e);
            document.getElementById("Lat").value = latLngValue.lat;
            document.getElementById("Lng").value = latLngValue.lng;
            //document.getElementById("destination").value = markerOptions.title;
            //$("#map_lat").val() = latValue;
        }
        /*function UpdateLatLng(e) {
            let latValue, lngValue;
            latValue = e.latlng.lat;
            lngValue = e.latlng.lng;

            console.log(e.latlng);
            document.getElementById("Lat").value = latValue;
            document.getElementById("Lng").value = lngValue;
            document.getElementById("destination").value = markerOptions.title;
            //$("#map_lat").val() = latValue;
        }*/

        /*function UpdateLatLng2(e) {
            let latLngValue = marker.getLatLng();
            latLngValue = position;

            document.getElementById("LatLng").value = latLngValue;
            newMarker.on('dragend', UpdateLatLng2);
            //$("#map_lat").val() = latValue;
        }*/



    </script>
    <!-- /.content -->
@endsection
