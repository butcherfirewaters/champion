<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome2');
})->name('home');*/
Route::group(['namespace' => 'Front'], function (){
    Route::get('/', 'TournamentController@main')->name('home');
    Route::post('/accept/{id}', 'TournamentController@acceptTournament')->name('acceptTournament');
    Route::get('/forallgamesfront', 'TournamentController@allgamesJson')->name('allgames');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
    Route::get('/', 'MainController@index')->name('admin.index');
    Route::resource('/categories', 'CategoryController');
    Route::resource('/tags', 'TagController');
    Route::resource('/posts', 'PostController');
    Route::resource('/tournaments', 'TournamentController');
    Route::get('/allpoints', 'TournamentController@allgamesJson')->name('tournament.allgames');
});

Route::group(['middleware' => 'guest'], function (){
    Route::get('/register', 'UserController@create')->name('register.create');
    Route::post('/register', 'UserController@store')->name('register.store');

    Route::get('/login', 'UserController@loginForm')->name('login.create');
    Route::post('/login', 'UserController@login')->name('login.store');

    Route::get('login/facebook', 'UserController@redirectToProvider')->name('login.facebook');
    Route::get('login/facebook/callback', 'UserController@handleProviderCallback');
});

Route::get('/logout', 'UserController@logout')->name('logout')->middleware('auth');

Route::get('/send', 'ContactController@sendEmail')->name('send.email');

Route::post('/logintotelegram/{id}', 'UserController@loginToTelegram')->name('sendlogin.telega');

Route::post("webhooks/telegram/1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs",
    ['as' => 'telegram.webhook', 'uses' => 'TelegramController@process']);

Route::get('/webhook', 'TelegramController@webhook');
