<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Telegram\Bot\Laravel\Facades\Telegram;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::any('/'.env('TELEGRAM_BOT_TOKEN'), 'TelegramController@index')->name('webhook');
Route::post('/1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs/webhook', function () {
    $messages = Telegram::getWebhookUpdates();
    $telegram = new \Telegram\Bot\Api('1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs');
    //https://api.telegram.org/bot1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs/setWebHook?url=https://298903aa0438.ngrok.io/api/1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs/webhook
    //dd($messages, $telegram);
    \Illuminate\Support\Facades\Log::info(json_encode($messages));
    foreach ($messages as $message){
        if ($message['chat']['id'] != null){
            $reply = "Спасибо, что подписались";
            $telegram->sendMessage(['chat_id' => $message['chat']['id'], 'text' => $reply]);
        }
        if ($message['from']['id'] != null){
            $reply = "Спасибо, что подписались";
            $telegram->sendMessage(['chat_id' => $message['from']['id'], 'text' => $reply]);
        }
        //\Illuminate\Support\Facades\Log::info($telegram);
        //dd($message);
        //$reply = "Спасибо, что подписались";
        //$telegram->sendMessage(['chat_id' => $message['from']['id'], 'text' => $reply]);
    }

    return 'ok';
});
