const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
    'resources/assets/admin/plugins/fontawesome-free/css/all.min.css',
    'resources/assets/admin/plugins/select2/css/select2.css',
    'resources/assets/admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.css',
    'resources/assets/admin/css/adminlte.min.css'
], 'public/assets/admin/css/admin.css');

mix.scripts([
    'resources/assets/admin/plugins/jquery/jquery.min.js',
    'resources/assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js',
    'resources/assets/admin/plugins/select2/js/select2.full.js',
    'resources/assets/admin/js/adminlte.min.js',
    'resources/assets/admin/js/demo.js'
], 'public/assets/admin/js/admin.js');

mix.copyDirectory('resources/assets/admin/img', 'public/assets/admin/img');
mix.copyDirectory('resources/assets/admin/plugins/fontawesome-free/webfonts', 'public/assets/admin/webfonts');

mix.copy('resources/assets/admin/css/adminlte.min.css.map', 'public/assets/admin/css/adminlte.min.css.map');
mix.copy('resources/assets/admin/js/adminlte.js.map', 'public/assets/admin/js/adminlte.min.js.map');

mix.styles([
    'resources/assets/front/css/bootstrap.css',
    'resources/assets/front/css/fonts/font-awesome-4.1.0/css/font-awesome.min.css',
    'resources/assets/front/css/own/owl.carousel.css',
    'resources/assets/front/css/own/owl.theme.css',
    'resources/assets/front/css/jquery.bxslider.css',
    'resources/assets/front/css/minislide/flexslider.css',
    'resources/assets/front/css/component.css',
    'resources/assets/front/css/prettyPhoto.css',
    'resources/assets/front/css/style_dir.css',
    'resources/assets/front/css/responsive.css',
    'resources/assets/front/css/animate.css',
], 'public/assets/front/css/frontmain.css');

mix.scripts([
    'resources/assets/front/js/jquery-1.10.2.js',
    'resources/assets/front/js/jquery-migrate-1.2.1.min.js',
    'resources/assets/front/js/jquery.transit.min.js',
    'resources/assets/front/js/menu/modernizr.custom.js',
    'resources/assets/front/js/menu/cbpHorizontalMenu.js',
    'resources/assets/front/js/minislide/jquery.flexslider.js',
    'resources/assets/front/js/circle/jquery-asPieProgress.js',
    'resources/assets/front/js/circle/rainbow.min.js',
    'resources/assets/front/js/gallery/jquery.prettyPhoto.js',
    'resources/assets/front/js/gallery/isotope.js',
    'resources/assets/front/js/jquery.ui.totop.js',
    'resources/assets/front/js/custom.js',
    'resources/assets/front/js/jquery.bxslider.js',
    'resources/assets/front/js/jquery.easing.1.3.js',
    'resources/assets/front/js/jquery.mousewheel.js',
    'resources/assets/front/js/own/owl.carousel.js',
    'resources/assets/front/js/jquery.countdown.js',
    'resources/assets/front/js/custom_ini.js',
], 'public/assets/front/js/frontmain.js');

mix.copyDirectory('resources/assets/front/img', 'public/assets/front/img');
mix.copyDirectory('resources/assets/front/images', 'public/assets/front/images');
