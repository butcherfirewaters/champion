<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Tournament extends Model
{

    use Notifiable;

    protected $fillable = [
        'title', 'destination', 'user_id', 'Lat', 'Lng', 'start_game', 'how_many_peoples'
    ];

    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function users(){
        return $this->belongsToMany(User::class);
    }
}
