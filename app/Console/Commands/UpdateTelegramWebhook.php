<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class UpdateTelegramWebhook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'telegram:webhook:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить данные webhook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
/*    public function handle()
    {
        $url = str_replace('http://', 'https://', route('telegram.webhook'));

        $telegramBot = Telegram::bot();

        $result = Telegram::bot()->setWebhook([
            'url' => $url,
        ]);

        dd($telegramBot, $telegramBot->getWebhookUpdate(), $result);

        if (!$result->getResult()){
            $this->error('Webhook установить не удалось :'. $result->getDecodedbody()['description']);
        }

        $this->info('Webhook успешно установлен');


    }*/
}
