<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/webhooks/telegram/1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs',
        '/1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs/webhook'
    ];
}
