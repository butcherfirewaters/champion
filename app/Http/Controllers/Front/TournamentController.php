<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Tournament;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TournamentController extends Controller
{
    public function main(){
        $user = Auth::user();

        $tournaments = Tournament::with('users')->get();

        if ($user) {
            return view('welcome2', compact('tournaments', 'user'));
        }

        return view('welcome2', compact('tournaments'));
    }

    public function acceptTournament($tournament){

        $user = Auth::check(); //Проверяем, есть ли Юзер
        //Находим нужный турнамент
        $findTournament = Tournament::find($tournament)->load('users');

        if($user){
            $user = Auth::user()->load('tournaments');

            //Есть ли уже у этого пользователя турниры, в которых он уже учавствует
            if ($user->tournaments()->count()){
                foreach ($user->tournaments as $userTournaments){
                    //dump($userTournaments->pivot->tournament_id);
                    if ($userTournaments->pivot->tournament_id == $findTournament->id){
                        return back()->with('success', 'Вы уже учавствуете в соревновании');
                    }
                }
            }

            //dd($user, $findTournament->id);


            //У пользователя нет турниров, и он принимает участие

            $user->tournaments()->syncWithoutDetaching($findTournament);
            return back()->with('success', 'Вы успешно учавствуете в игре');
        }

        //Для незарегистрированных пользователей
        else{
            return back()->with('success', 'Чтобы принять участие, Вам нужно авторизоваться');
        }

    }

    public function allgamesJson(){
        $tournaments = Tournament::all();
        return json_encode($tournaments, JSON_UNESCAPED_UNICODE);
    }
}
