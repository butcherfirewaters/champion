<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Str;
use App\Notice;
use Illuminate\Notifications\Notifiable;
use App\Notifications\TelegramNotification;
use NotificationChannels\Telegram\TelegramChannel;
use Ramsey\Uuid\Uuid;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;

class UserController extends Controller
{

    use Notifiable;

    public function create(){
        /*$telegram = new Api('1435262695:AAFxXNUCfk7n2tp9-o97lvlZg-XeKWk4DFo');

        /*$botId = $response->getId();
        $firstName = $response->getFirstName();
        $username = $response->getUsername();*/

        return view('user.create');
    }

    public function sendMessageToTelega($uri, $chat_id = ''){
        $uri = 'https://api.telegram.org/bot1435262695:AAFxXNUCfk7n2tp9-o97lvlZg-XeKWk4DFo/sendMessage?chat_id=581654009&text=Я%20ЗНАЮ%20ЧТО%20делал%20прошлым%20летом';

    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'customRadio' => 'required'
        ]);

        $password = bcrypt($request->password);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password,
            'is_admin' =>$request->customRadio
        ]);

        $notice = new Notice([
            'id' => Uuid::uuid4()->toString(),
            'notice' => 'Вы успешно зарегистрировались',
            'noticedes' => $user->email. "+".$user->name,
            'noticelink' => 'https://champion.loc',
            'telegramid' => '581654009' // мой ID
        ]);

        $notice->save();
        $notice->notify(new TelegramNotification());

        session()->flash('success', 'Вы успешно зарегистрировались');
        Auth::login($user);
        return redirect()->home();
    }



    public function loginForm(){
        return view('user.login');
    }

    public function login(Request $request){

        $request->validate([
            'email' => 'required|email|',
            'password' => 'required',
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            session()->flash('success', 'You are logged');
            if (Auth::user()->is_admin){
                return redirect()->route('admin.index');
            }
            else {
                return redirect()->home();
            }
        }

        return redirect()->back()->with('error', 'Incorrect login or password');

    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        //получаем Юзера из соцсети
        $socialiteUser = Socialite::driver('facebook')->stateless()->user();

        // Проверяем, если ли пользователь или нет и создаём нового
        $user = User::firstOrCreate([
            'email' => $socialiteUser->email
        ], [
           'name' => $socialiteUser->name,
           'password' => Hash::make(Str::random(8)),
           'is_admin' => '0'
        ]);
        //dd($socialiteUser, $user);

        //Дальше понятно, передаём флеш сообщение и логимся в систему
        session()->flash('success', 'Вы успешно зарегистрировались или вошли в ситему');
        Auth::login($user);
        return redirect()->home();

    }

    public function loginToTelegram(){

        $telegram = new Api(Config::get('telegram.bots.mybot.token', 'default'));
        $botTelegi = Telegram::bot()->getWebhookUpdate();

        $response = $telegram->getMe();
        $botId = $response->getId();

        $newMessage = $telegram->sendMessage(['chat_id' => '581654009', 'text' => 'Здарова!']);
        //$kolya = $telegram->setWebhook(['url'=> 'https://det.dn.ua']);

        dd($response, $botId, $botTelegi, $newMessage['chat']['id']);

        $route = 'https://api.telegram.org/bot1435262695:AAEI_W2pLAaXDqnegj4LQi9uAJAziK4qxvs/getUpdates';

        return $route;
    }

    public function webhookTelegram(User $id, Request $request){
        //$requestArray = $request->all();
        $telegram = new Api(Config::get('telegram.bots.mybot.token', 'default'));
        $deleteweb = $telegram->deleteWebhook();
        $result = $telegram->getWebhookUpdates();


        $text = $result["message"]["text"];
        $chat_id = $result["message"]["chat"]["id"];

        $name = $result["message"]["from"]["username"];
        $first_name = $result["message"]["from"]["first_name"];

        if ($text == "/start"){
            $reply = "HelloWorld";
            $telegram->sendMessage(['chat_id' => $chat_id, 'text' => $reply]);
        }

        return $text;
        dd($request->all(), $result, $id);
        try {
            Telegram::sendMessage([
                'chat_id' => '581654009',
                'text' => 'Пожалуйста для завершения подписки перейдите по ссылке'
            ]);
        } catch (\Exception $exception) {

        }
    }

    public function logout(){
        Auth::logout();
        session()->flash('success', 'Вы успешно вышли из аккаунта');
        return redirect()->route('login.create');
    }
}
