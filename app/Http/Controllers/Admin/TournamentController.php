<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\TestMail;
use App\Notice;
use App\Tournament;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;
use App\Notifications\LocateTelegram;
use App\Notifications\TelegramNotification;
use Ramsey\Uuid\Uuid;

class TournamentController extends Controller
{
    use Notifiable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tournaments = Tournament::all();
        return view('admin.tournaments.index', compact('tournaments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tournaments = Tournament::all();
        $players = User::where('is_admin', 0)->get();
        $user = Auth::user();
        return view('admin.tournaments.create', compact('tournaments', 'user', 'players'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date_time_now = date("Y-m-d").'T'.date("H:i");
        //dd($date_time_now);

        $request->validate([
            'title' => 'required|max:255',
            'user_id' => 'integer',
            'destination' => 'required|max:255',
            'Lat' => 'required|numeric',
            'Lng' => 'required|numeric',
            /*'tournaments.user' => 'required',*/
            'how_many_peoples' => 'required|integer|min:8|max:20',
            'start_game' => 'required|after_or_equal:'.$date_time_now
        ]);

        $data = $request->all();
        $user = Auth::user();



        $tournament = Tournament::make($data);

        if ($request->players){
            $massive_players = $request->players;
            if (count($massive_players) > $tournament->how_many_peoples){
                session()->flash('error', 'Вы выбрали недопустимое количество игроков');
                $vasia = session()->all();
                return back()->withInput()->with('error', $vasia['error']);
            }
        }

        $tournament->save();

        $tournament->users()->sync($request->players);

        /* Для отправки писем*/

        $players_for_tournament = $tournament->users;

        $this->sendMailForTournament($players_for_tournament, $tournament);

        $notice = new Notice([
            'id' => Uuid::uuid4()->toString(),
            'notice' => 'Вы приглашены принять участие в турнире по футболу',
            'noticedes' => "Который будет проходить в " .$tournament->destination. " Время и дата - ".$tournament->start_game,
            'noticelink' => 'https://champion.loc',
            'telegramid' => '581654009' // мой ID //Руслана 387200612
        ]);

        $notice->save();
        $notice->notify(new LocateTelegram());
        $notice->notify(new TelegramNotification());


        /*Закончили писать для отправки писем*/

        return redirect()->route('tournaments.index')->with('success', 'Игра по футболу создана');

    }

    public function sendMailForTournament($players_for_tournament, $tournament){
        foreach ($players_for_tournament as $onePlayer){
            Mail::to($onePlayer->email)->send(new TestMail($tournament));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function allgamesJson(){
        $tournaments = Tournament::all();
        return json_encode($tournaments, JSON_UNESCAPED_UNICODE);
    }
}
