<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\TournamentController;
use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{

    /*public function sendEmail(){
        $pols = ['collection' =>['vasia'=> 'firewatersoffice@gmail.com', 'petya'=> 'firewatersoffice3@gmail.com', 'kolya'=> 'firewatersoffice6@gmail.com']];
        foreach ($pols['collection'] as $pol){
            dump($pol);
            Mail::to($pol)->send(new TestMail());
        }
        return view('email.send');
    }*/

    public function sendMailForTournament($players_for_tournament, $tournament){
        foreach ($players_for_tournament as $onePlayer){
            Mail::to($onePlayer->email)->send(new TestMail($tournament));
        }
    }

}
